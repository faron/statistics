import java.io.FileNotFoundException;
import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;


class Position {
    class WrongPosition extends Exception {
    }

    public int x, y;
    private final Character[] letterCoordinates = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};

    public Position(String name) throws WrongPosition {
        char[] coordinates = name.toCharArray();
        x = letterToInt(coordinates[0]);
        y = Character.getNumericValue(coordinates[1]);
        if (x < 1 || x > 8 || y < 1 || y > 8) {
            throw new WrongPosition();
        }
    }

    private int letterToInt(char letter) throws WrongPosition {
        int value = Arrays.asList(letterCoordinates).indexOf(letter);
        if (value == -1) {
            throw new WrongPosition();
        }
        return value + 1; // Adjusted to start with 1
    }
}

class Move {
    public Position from, to;

    public Move(String description) throws Position.WrongPosition {
        String[] positionDescriptions = description.split("-");
        from = new Position(positionDescriptions[0]);
        to = new Position(positionDescriptions[1]);
    }
}

class FileReader {
    private String fileName;

    public FileReader(String fileName) throws FileNotFoundException, Position.WrongPosition {
        this.fileName = fileName;
    }

    public Move read() throws FileNotFoundException, Position.WrongPosition {
        Scanner scan = new Scanner(new File(fileName));
        Move move = new Move(scan.next());
        scan.close();
        return move;
    }

}

class MoveChecker {
    String isValid(Move move) {
        int deltaX = Math.abs(move.from.x - move.to.x);
        int deltaY = Math.abs(move.from.y - move.to.y);
        String answer;
        // Valid moves are with deltas 1 and 2 on either coordinate
        if ((deltaX == 1 && deltaY == 2) || (deltaX == 2 && deltaY == 1)) {
            answer = "YES";
        } else {
            answer = "NO";
        }
        return answer;
    }

}

class FileWriter {
    private String fileName;

    public FileWriter(String fileName) {
        this.fileName = fileName;
    }

    public void write(String result) throws FileNotFoundException {
        File newFile = new File(fileName);
        PrintWriter writer = new PrintWriter(newFile);
        writer.write(result);
        writer.flush();
    }
}

public class Main {
    final static String INPUT_FILE = "INPUT.TXT";
    final static String OUTPUT_FILE = "OUTPUT.TXT";

    public static void main(String[] args) throws FileNotFoundException {
        FileWriter writer = new FileWriter(OUTPUT_FILE);

        try {
            Move move = new FileReader(INPUT_FILE).read();
            writer.write(new MoveChecker().isValid(move));
        } catch (Exception e) {
            writer.write("ERROR");
        }
    }

}
