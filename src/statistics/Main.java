//package statistics;

import java.io.FileNotFoundException;

import java.io.File;
import java.util.Scanner;
import java.io.PrintWriter;

public class Main {


    static class FileWriter {
        private PrintWriter writer;
        private File newFile;
        private String oddDays;
        private String evenDays;
        private String answer;

        public FileWriter(String outPutFile, String theAnswer, String odd, String even) throws FileNotFoundException {
            newFile = new File(outPutFile);
            writer = new PrintWriter(newFile);
            oddDays = odd;
            evenDays = even;
            answer = theAnswer;
        }

        public void writeIntoOutPutFile() {
            writer.write(oddDays);
            writer.write("\n");
            writer.write(evenDays);
            writer.write("\n");
            writer.write(answer);
            writer.flush();
        }

    }

    static class FileReader {
        private int arrayLength;
        private int[] array;

        public FileReader(String inputFile) throws FileNotFoundException {
            readFile(inputFile);
        }

        private void readFile(String inputFile) throws FileNotFoundException {
            Scanner scan = new Scanner(new File(inputFile));
            arrayLength = scan.nextInt();
            array = new int[arrayLength];

            int i = 0;
            while (scan.hasNext()) {
                array[i++] = scan.nextInt();
            }

            scan.close();
        }

        public int[] returnArray() {

            return array;
        }

    }

    static class Calculator {
        private int[] array;
        private String answer;
        private String odd = "";
        private String even = "";
        private int oddNumCounter = 0;
        private int evenNumCounter = 0;

        public Calculator(int[] array) {
            this.array = array;
            gettingEvenOddnums();
            gettingAnswer();
            System.out.println(gettingAnswer());
            System.out.println(odd + " " + "\n" + even + " ");
        }

        private void gettingEvenOddnums() {
            for (int i = 0; i < array.length; i++) {
                if (array[i] % 2 == 0) {
                    even = even + " " + array[i];
                    evenNumCounter++;
                } else {
                    odd = odd + " " + array[i];
                    oddNumCounter++;
                }
            }
        }

        private String gettingAnswer() {
            if (evenNumCounter >= oddNumCounter) {
                answer = "YES";
            } else {
                answer = "NO";
            }
            return answer;
        }

        public String getAnswer() {
            return answer;
        }

        public String getOdd() {
            return odd;
        }

        public String getEven() {
            return even;
        }

    }

    static class ArrayOfIntegers {

        private int[] array;
        private int arrayLength;

        public ArrayOfIntegers(int length) {
            arrayLength = length;
            array = new int[arrayLength];
        }

        public void fillArray(int[] array) {
            this.array = array;
        }
    }

    private final static String INPUT_FILE = "INPUT.TXT";
    private final static String OUTPUT_FILE = "OUTPUT.TXT";

    public static void main(String[] args) throws FileNotFoundException {
        FileReader reader = new FileReader(INPUT_FILE);
        Calculator calculator = new Calculator(reader.returnArray());

        FileWriter writer = new FileWriter(OUTPUT_FILE, calculator.getAnswer(), calculator.getOdd(), calculator.getEven());
        writer.writeIntoOutPutFile();
    }
}
