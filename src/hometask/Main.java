package hometask;

import java.io.FileNotFoundException;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;


public class Main {

    static class FileReader {
        private int arrayLength;
        private int[] array;

        public FileReader(String inputFile) throws FileNotFoundException {
            readFile(inputFile);
        }

        private void readFile(String inputFile) throws FileNotFoundException {
            Scanner scan = new Scanner(new File(inputFile));
            arrayLength = scan.nextInt();
            array = new int[arrayLength];

            int i = 0;
            while (scan.hasNext()) {
                array[i++] = scan.nextInt();
            }
            scan.close();
        }

        public int[] returnArray() {

            return array;
        }

    }

    static class Calculator {
        private int[] array;
        private int max;
        private int min;
        private int j;//index of max
        private int k;//index of min

        public Calculator(int[] array) {
            this.array = array;
            findMax();
            findMinimun();
        }

        private int findMinimun() {
            min = array[0];
            for (int i = 0; i < array.length; i++) {
                if (array[i] < min) {
                    min = array[i];
                    k = i;
                }
            }
            return min;
        }

        private int findMax() {
            max = array[0];
            for (int i = 0; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                    j = i;
                }
            }
            return max;
        }

        public int getComposition() {
            int number = 1;
            if (j > k) {
                for (int i = 0; i < array.length; i++) {
                    if (array[i] > min && array[i] < max && i > k && i < j) {
                        number *= array[i];
                    }
                }
            } else {
                for (int i = array.length - 1; i > 0; i--) {
                    if (array[i] > min && array[i] < max && i < k && i > j) {
                        number *= array[i];
                    }
                }
            }
            return number;
        }

        public int getSumPositiveNum() {
            int sum = 0;
            for (int i = 0; i < array.length; i++) {
                if (array[i] >= 0) {
                    sum += array[i];
                }
            }
            return sum;
        }
    }



    static class FileWriter {
        private PrintWriter writer;
        private File newFile;
        private int composition;
        private int sum;

        public FileWriter(String outputFile, int composit, int summ) throws FileNotFoundException {
            newFile = new File(outputFile);
            writer = new PrintWriter(newFile);
            composition = composit;
            sum = summ;
        }

        public void write() {
            writer.write(String.valueOf(sum) + " ");
            writer.write(String.valueOf(composition));
            writer.flush();


        }

    }

    private final static String INPUT_FILE = "INPUT.TXT";
    private final static String OUTPUT_FILE = "OUTPUT.TXT";

    public static void main(String[] args) throws FileNotFoundException {

        FileReader reader = new FileReader(INPUT_FILE);
        Calculator calculator = new Calculator(reader.returnArray());
        FileWriter writer = new FileWriter(OUTPUT_FILE, calculator.getComposition(), calculator.getSumPositiveNum());
        writer.write();


    }
}
